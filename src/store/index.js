import {
  createStore
} from 'vuex'

export default createStore({
  state: {
    todos: [],
  },
  mutations: {
    //Добавляем в state значения из localsorage
    addTodoFromLocalstorage(state) {
      let todosLocalstorage = JSON.parse(localStorage.getItem('todos'));
      if (todosLocalstorage) {
        state.todos = todosLocalstorage;
      }
    },

    //Добавляем todo в список todos
    addTodo(state, payload) {
      if (localStorage.getItem('todos')) {
        let arrayLocal = JSON.parse(localStorage.getItem('todos'));
        arrayLocal.push(payload);
        localStorage.setItem('todos', JSON.stringify(arrayLocal));
      } else {
        localStorage.setItem('todos', JSON.stringify([payload]));
      }
      state.todos.push(payload)
    },

    //Удаление todo
    removeTodo(state, payload) {
      function removeByAttr(arr, attr, value) {
        let i = arr.length;
        while (i--) {
          if (arr[i] && arr[i].hasOwnProperty(attr) && (arguments.length > 2 && arr[i][attr] === value)) {
            arr.splice(i, 1);
          }
        }
        return arr;
      }

      removeByAttr(state.todos, 'key', payload);

      let todosLocalstorage = JSON.parse(localStorage.getItem('todos'));

      todosLocalstorage.forEach(function (item, index) {
          if (item['key'] === payload) {
              todosLocalstorage.splice(index, 1)
          }
      });

      localStorage.setItem('todos', JSON.stringify(todosLocalstorage));
    },

    //Добавление отредактированного todo в список todos
    addEditTodo(state, payload) {
      state.todos.forEach((item, i) => {
        if (item.key === payload.key) {
          state.todos[i] = payload;
        }
      })

      let todosLocalstorage = JSON.parse(localStorage.getItem('todos'));

      todosLocalstorage.forEach((item) => {

          if (item['key'] === payload.key) {
              item['title'] = payload.title;
              item['description'] = payload.description;
              item['priority'] = payload.priority;
          }
      });

      localStorage.setItem('todos', JSON.stringify(todosLocalstorage));
    }

  },
  getters: {
    getTodo: state => {
      return state.todo
    },

    //Получение списка незавершенных todo с параллельной сортировкой по важности задачи
    unfinishedTodo: state => {
      state.todos.sort((a, b) => a.priority < b.priority ? 1 : -1);
      return state.todos.filter(item => item.done === false)
    },


    //Получение списка завершенных todo с параллельной сортировкой по важности задачи
    doneTodo: state => {
      state.todos.sort((a, b) => a.priority < b.priority ? 1 : -1);
      return state.todos.filter(item => item.done === true)
    }
  }
})